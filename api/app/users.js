const express = require('express');
const auth = require('../middlweare/auth');

const User = require('../models/User');

const router = express.Router();



router.post('/', async (req,res) => {
    console.log(req.body);
    const user = new User(req.body);
    user.generateToken();
    try {
        await user.save();
        return res.send({token: user.token})
    } catch (error) {
        return res.status(400).send(error)
    }

});


router.post('/sessions', async (req,res) => {

    const user = await User.findOne({username: req.body.username});

    if (!user){
        return res.status(400).send({error: 'Username not found'})
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
        return res.status(400).send({error: 'Password is wrong'})
    }

    user.generateToken();
    await  user.save();

    res.send(user)
});


router.delete('/sessions', async (req,res) => {
    const token = req.get('Authorization');
    const success = {message:'Logged out' };
    if (!token){
        return res.send(success)
    }

    const user = await User.findOne({token});
    if (!user){
        return res.send(success)
    }

    user.generateToken();
    await user.save();

    return res.send(success)

});


router.put('/', auth, async (req, res) => {
    this.user.password = req.body.password;

    await  req.user.save();
    res.sendStatus(200);
});


module.exports = router;