const mongoose = require('mongoose');
const config = require('./config');
const Category = require('./models/Category');

const Product = require('./models/Product');
const User = require('./models/User');


const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const users = await User.create(
        {
            username: "jack_frost",
            password: "123",
            DisplayName: "jack Frost",
            NumberPhone: +996700765678
        },
        {
            username: "jin_min",
            password: "123",
            DisplayName: "jin Min",
            NumberPhone: +99655055003
        },
        {
            username: "kris_wu",
            password: "123",
            DisplayName: "Kris Wu",
            NumberPhone: +996553553333
        }

    );

    const categories = await Category.create(
        {title: 'CPUs', description: 'Central Processing Units', token: 'yeuqukck',},
        {title: 'HDDs', description: 'Hard Disk Drives', token: 'yeuqukckca',},
        {title: 'STE', description: 'System Unit', token: 'yduavdlkjaj',}
    );



    await Product.create(
        {
            title: ' system unit Regard RE750',
            price: 24999,
            description: 'Very cool STE',
            category: categories[2]._id,
            user: users[0]._id,
            image: 'stm.jpg'
        },
        {
            title: 'Intel Core i7',
            price: 500,
            description: 'Very cool CPU',
            category: categories[0]._id,
            image: 'cpu.jpg',
            user: users[1]._id,
        },
        {
            title: 'Toshiba 500GB',
            price: 60,
            description: 'Just a simple HDD',
            category: categories[1]._id,
            image: 'hdd.jpg',
            user: users[2]._id,
        }
    );

    await connection.close();
};

run().catch(error => {
    console.error('Something went wrong', error);
});

