import React, {Fragment} from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, NavItem, NavLink, UncontrolledDropdown} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

const UserMenu = ({user, logout}) => (
    <Fragment>
        <NavItem>
            <NavLink tag={RouterNavLink} to="/add_product" exact>Add New Product</NavLink>
        </NavItem>
        <UncontrolledDropdown nav inNavbar>
            <DropdownToggle nav caret>
                Hello, {user.username}
            </DropdownToggle>
            <DropdownMenu right>
                <DropdownItem>
                    options
                </DropdownItem>
                <DropdownItem divider />
                <DropdownItem onClick={logout} >
                    Logout
                </DropdownItem>
            </DropdownMenu>
        </UncontrolledDropdown>
    </Fragment>

);

export default UserMenu;