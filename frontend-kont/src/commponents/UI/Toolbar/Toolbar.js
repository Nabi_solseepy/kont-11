import React from 'react';
import {NavLink as RouterNavLink} from "react-router-dom";
import {Nav, Navbar, NavbarBrand,} from "reactstrap";
import AnonymousMenu from "./Menu/AnonymousMenu";
import UserMenu from "./Menu/UserMenu";

const Toolbar = ({user, logout}) => {
    return (
        <Navbar color="primary" dark expand="md">
            <NavbarBrand tag={RouterNavLink} to='/' >Forum</NavbarBrand>

            <Nav className="ml-auto" navbar>
                {user ? <UserMenu user={user} logout={logout}/> : <AnonymousMenu/>}
            </Nav>
        </Navbar>
    );
};

export default Toolbar;