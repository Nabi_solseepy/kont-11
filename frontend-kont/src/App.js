import React, {Fragment, Component} from 'react';
import './App.css';
import Toolbar from "./commponents/UI/Toolbar/Toolbar";
import {Container} from "reactstrap";
import {Route, Switch, withRouter} from "react-router-dom";
import Login from "./containers/Login/Login";
import ProtuctsLink from "./containers/ProtuctLink/ProtuctsLink";
import {connect} from "react-redux";
import {logoutUser} from "./store/actions/userAction";
import Register from "./containers/Registor/Register";
import {NotificationContainer} from "react-notifications";
import AddNewProduct from "./containers/AddNewProduct/AddNewProduct";
import Item from "./containers/Item/Item";

class App extends Component{
    render() {
        return (
            <Fragment>
                <NotificationContainer/>
                <Toolbar user={this.props.user} logout={this.props.logoutUser}/>
                <Container style={{marginTop: '20px'}}>
                    <Switch>
                        <Route path="/add_product" exact component={AddNewProduct} />
                        <Route path="/register" exact component={Register}/>
                        <Route path="/" exact component={ProtuctsLink}/>
                        <Route path="/login" exact component={Login}/>
                        <Route path='/product/:id' exact component={Item}/>
                    </Switch>
                </Container>
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    logoutUser: () => dispatch(logoutUser())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
