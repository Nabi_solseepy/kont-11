import React, {Component, Fragment} from 'react';
import {Button, Card, CardBody, CardImg, CardSubtitle, CardText, CardTitle} from "reactstrap";
import {connect} from "react-redux";
import {fetchItem} from "../../store/actions/productAction";

class Item extends Component {
    componentDidMount() {
        this.props.fetchItem(this.props.match.params.id)
    }

    render() {
        return (
            <Fragment>
            {this.props.item && (
                    <Card>
                        <CardImg top width="100%" src={"http://localhost:8000/uploads/" + this.props.item.image} alt="Card image cap" />
                        <CardBody>
                            <CardTitle>{this.props.item.title}</CardTitle>
                            <CardSubtitle>{this.props.item.price}</CardSubtitle>
                            <CardText>{this.props.item.description}</CardText>
                            <CardText>{this.props.item.user.DisplayName}</CardText>
                            <CardText>{this.props.item.user.NumberPhone}</CardText>
                            <Button>Delete</Button>
                        </CardBody>
                    </Card>
                )}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    item: state.products.item
});



const mapDispatchToProps = dispatch => ({
    fetchItem: (id) => dispatch(fetchItem(id))

});

export default connect(mapStateToProps, mapDispatchToProps )(Item);