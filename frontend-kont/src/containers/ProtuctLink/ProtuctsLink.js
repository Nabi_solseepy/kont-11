import React, {Component,Fragment} from 'react';
import {
    Col,
    Nav,
    NavItem,
    NavLink,
    Row
} from "reactstrap";
import {NavLink as RouterNavLink, Route} from "react-router-dom";
import {connect} from "react-redux";
import {fetchCategories} from "../../store/actions/categoryAction";
import Products from "../Products/Products";

class ProtuctsLink extends Component {
    componentDidMount() {
        this.props.fetchCategories()
    }

    render() {
        return (
            <Fragment>
                <Fragment>
                    <Row>
                        <Col sm={4}>
                            <Nav vertical>
                                <NavItem >
                                    <NavLink tag={RouterNavLink} to="/" exact >All items</NavLink>
                                </NavItem>
                                {this.props.categories.map(category => (
                                    <NavItem key={category}>
                                        <NavLink exact tag={RouterNavLink} to={"/product/" + category._id}>{category.title}</NavLink>
                                    </NavItem>
                                ))}
                            </Nav>
                        </Col>
                        <Col sm={8}>
                            <Route path="/" exact component={Products}/>
                        </Col>
                    </Row>

                </Fragment>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    categories:  state.categories.categories
});

const mapDispatchToProps = dispatch => ({
    fetchCategories: () => dispatch(fetchCategories())
});

export default connect(mapStateToProps, mapDispatchToProps)(ProtuctsLink);