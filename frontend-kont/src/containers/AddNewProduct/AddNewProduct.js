import React, {Component} from 'react';
import Form from "reactstrap/es/Form";
import FormElement from "../../commponents/UI/Form/FormElement";
import {Button, Col, FormGroup, Input, Label} from "reactstrap";
import {createNewProduct} from "../../store/actions/productAction";
import {connect} from "react-redux";
import {fetchCategories} from "../../store/actions/categoryAction";

class AddNewProduct extends Component {
    state = {
        title: '',
        description: '',
        image: '',
        category: '',
        price: '',
    };


    componentDidMount() {
        this.props.fetchCategories()
    }

    inputFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key])

        });
      this.props.createNewProduct(formData)

    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };


    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    render() {
        return (
           <Form onSubmit={this.inputFormHandler}>
               <FormElement
                   propertyName='title'
                   title='Title'
                   type='text'
                   value={this.state.title}
                   onChange={this.inputChangeHandler}/>

               <FormElement
                   propertyName="description"
                   title="Description"
                   type="textarea"
                   value={this.state.description}
                   onChange={this.inputChangeHandler}
               />
               <FormElement
                   propertyName="price"
                   title="Price"
                   type="number"
                   onChange={this.inputChangeHandler}
               />
               <FormElement
                   propertyName="image"
                   title="Image"
                   type="file"
                   onChange={this.fileChangeHandler}
               />


               <FormGroup row>
                   <Label sm={2} for="category">Category</Label>
                   <Col sm={10}>
                       <Input
                           type="select" required
                           name="category" id="category"
                           value={this.state.category}
                           onChange={this.inputChangeHandler}
                       >
                           {this.props.categories.map(category => (
                               <option key={category._id} value={category._id}>{category.title}</option>
                           ))}
                       </Input>
                   </Col>
               </FormGroup>
               <FormGroup row>
                   <Col sm={{offset: 2, size: 10}}>
                       <Button type="submit" color="primary">Save</Button>
                   </Col>
               </FormGroup>
           </Form>
        );
    }
}

const mapStateToProps = state => ({
    categories: state.categories.categories
});

const mapDispatchToProps = dispatch => ({
    createNewProduct: (productData) => dispatch(createNewProduct(productData)),
    fetchCategories: () => dispatch(fetchCategories())
});


export default connect(mapStateToProps, mapDispatchToProps)(AddNewProduct);