import React, {Component, Fragment} from 'react';
import {Card, CardBody, CardImg, CardSubtitle, CardText, } from "reactstrap";
import {connect} from "react-redux";
import {fetchProducts} from "../../store/actions/productAction";
import {Link} from "react-router-dom";
import CardColumns from "reactstrap/es/CardColumns";


class Products extends Component {
    componentDidMount() {
        this.props.fetchProducts()
    }

    render() {
        return (
            <Fragment>
                <CardColumns>
                {this.props.products.map(product =>(
                  <Link key={product._id} to={"/product/" + product._id}>
                    <Card>
                        <CardImg top width="100%" src={"http://localhost:8000/uploads/" + product.image} alt="Card image cap" />
                        <CardBody>
                            <CardSubtitle>{product.title}</CardSubtitle>
                            <CardText>{product.price}</CardText>
                        </CardBody>
                    </Card>
                  </Link>
                ))}
                </CardColumns>
            </Fragment>
        );
    }
}


const mapStateToProps = state => ({
   products: state.products.products
});

const mapDispatchToProps = dispatch => ({
    fetchProducts: () => dispatch(fetchProducts())
});

export default connect(mapStateToProps, mapDispatchToProps)(Products);