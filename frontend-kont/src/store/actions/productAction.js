import axios from '../../axios-api';
import {push} from "connected-react-router";

export const FETCH_PRODUCTS_SUCCESS = 'FETCH_PRODUCTS_SUCCESS';

export const FETCH_PRODUCT_ONE_SUCCESS = 'FETCH_PRODUCT_ONE_SUCCESS';

export const CREATE_NEW_PRODUCT_SUCCESS = 'CREATE_NEW_PRODUCT_SUCCESS';

export const FETCH_ITEM_SUCCESS = 'FETCH_ITEM_SUCCESS';



export const fetchProductsSuccess = (products) => ({type: FETCH_PRODUCTS_SUCCESS, products});


export const createNewProductSuccess = () => ({type: CREATE_NEW_PRODUCT_SUCCESS});

export const fetchItemSuccess = (item) => ({type: FETCH_ITEM_SUCCESS, item});

export const fetchProducts = () => {
    return dispatch => {
        axios.get('/products').then(
            response => {
                dispatch(fetchProductsSuccess(response.data))
            }
        )
    }
};


export const fetchItem = (id) => {
  return dispatch => {
      axios.get('/products/' + id).then(
          response => {
              dispatch(fetchItemSuccess(response.data))
          }
      )
  }
};


export const createNewProduct = (productData) => {
    return (dispatch, getState) => {
        const user = getState().users.user;
        if (!user ){
            dispatch(push('/login'))
        } else {
            axios.post('/products',productData, {headers: {'Authorization': user.token}}).then(
                () => {
                    dispatch(createNewProductSuccess());
                    dispatch(push('/'))
                }

            )
        }

    }
};


