import axios from '../../axios-api';

export const FETCH_CATEGORIES_SUCCESS = 'FETCH_COTEGORIES_SUCCESS ';


export const fetchCategoriesSuccess = categories  => ({type: FETCH_CATEGORIES_SUCCESS, categories});



export const fetchCategories = () => {
    return dispatch => {
        return axios.get('/products?categories').then(response => {
            dispatch(fetchCategoriesSuccess(response.data))
        })
    }
};