import {connectRouter, routerMiddleware} from "connected-react-router";
import thunkMiddleware from "redux-thunk";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {createBrowserHistory} from "history";
import userReducer from "./reducer/userReducer"
import categoryReducer from "./reducer/categoryReducer";
import productReducer from "./reducer/productReducer"
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorge";

export const history = createBrowserHistory();

const rootReducer = combineReducers({
    router: connectRouter(history),
    users: userReducer,
    categories: categoryReducer,
    products: productReducer


});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middleware = [
    thunkMiddleware,
    routerMiddleware(history)
];

const enhancers = composeEnhancers(applyMiddleware(...middleware));

const persistedState = loadFromLocalStorage();

const store = createStore(rootReducer, persistedState, enhancers);


store.subscribe(()=> {
    saveToLocalStorage({
        users: {
            user: store.getState().users.user
        }
    })
});



export default store;