import {
    FETCH_ITEM_SUCCESS,
    FETCH_PRODUCT_ONE_SUCCESS,
    FETCH_PRODUCTS_SUCCESS
} from "../actions/productAction";

const initialState = {
    products: [],
    product: null,
    item:null

};

const productReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PRODUCTS_SUCCESS:
            return {...state, products: action.products};
        case FETCH_PRODUCT_ONE_SUCCESS:
            return{...state, product: action.product};
        case FETCH_ITEM_SUCCESS:
            return{...state, item: action.item};
        default:
            return state
    }
};


export default productReducer